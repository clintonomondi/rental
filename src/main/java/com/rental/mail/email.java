package com.rental.mail;

import java.util.List;

import org.apache.velocity.VelocityContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rental.common.EmailManager;






@SpringBootApplication
@RequestMapping("/api")
public class email {

	@RequestMapping(value = "/v1/email", method = RequestMethod.POST)
	 public ResponseEntity<?> email(@RequestBody mailModel jsondata){
		
		 Thread t = new Thread() {
				public void run() {
		try {
		

			 VelocityContext vc = new VelocityContext();
	          vc.put("message", jsondata.getMessage());
	          vc.put("subject", jsondata.getSubject());
	          String email_template = EmailManager.email_message_template(vc,"email.vm");
				EmailManager.send_mail(jsondata.getEmail(),jsondata.getSubject(),email_template);
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		}};
		t.start(); 
		return null;
	 }
}
