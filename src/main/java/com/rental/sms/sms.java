package com.rental.sms;

import java.util.Hashtable;
import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.africastalking.AfricasTalking;
import com.africastalking.SmsService;
import com.africastalking.sms.Recipient;
import com.rental.common.ExternalFile;


@SpringBootApplication
@RequestMapping("/api")
public class sms {
	// Initialize
	 // use your sandbox app API key for development in the test environment
	
	@RequestMapping(value = "/v1/sms", method = RequestMethod.POST)
	 public ResponseEntity<?> sms(@RequestBody smsModel jsondata){
		
		 Thread t = new Thread() {
				public void run() {
		String username = ExternalFile.getAFT_USERNAME();    // use 'sandbox' for development in the test environment
		String apiKey = ExternalFile.getAFT_APIKEY();      
		try {
		String phone="+254"+jsondata.getPhone().substring(1);
		String message=jsondata.getMessage();
		AfricasTalking.initialize(username, apiKey);
		SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);
		List<Recipient> response = sms.send(message, new String[] {phone}, true);
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
				}
			};
			t.start(); 
		return null;
	 }
}
