package com.rental.common;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import org.apache.commons.codec.binary.Base64;

public class DESEncryption {
	private static final String UNICODE_FORMAT = "UTF8";
	public static final String DES_ENCRYPTION_SCHEME = "DES";
//	private static Cipher cipher=null;
	static byte[] keyAsBytes=null;
	static SecretKey key=null;
	static String myEncryptionKey = "U5cp5ywS7byc8b75z8uF95swojlrXNX6";
	static String myEncryptionScheme = DES_ENCRYPTION_SCHEME;

public static String decrypt(String encryptedString) {
		
	 Cipher cipher=null;
		byte[] keyAsBytes;
		SecretKey key;
		String decryptedText = null;
		try {
			
			keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
			KeySpec myKeySpec = new DESKeySpec(keyAsBytes);
			SecretKeyFactory mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
			cipher = Cipher.getInstance(myEncryptionScheme);
			key = mySecretKeyFactory.generateSecret(myKeySpec);
			cipher.init(Cipher.DECRYPT_MODE, key);
			//BASE64Decoder base64decoder = new BASE64Decoder();
			//byte[] encryptedText = base64decoder.decodeBuffer(encryptedString);
			byte[] encryptedText=Base64.decodeBase64(encryptedString);
			byte[] plainText = cipher.doFinal(encryptedText);
			decryptedText = bytes2String(plainText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decryptedText;
	}

	/**
	 * Returns String From An Array Of Bytes
	 */
	private static String bytes2String(byte[] bytes) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			stringBuffer.append((char) bytes[i]);
		}
		return stringBuffer.toString();
	}
	
	public static String encrypt(String unencryptedString) {
		String encryptedString = null;
		 Cipher cipher=null;
		try {
			keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
			KeySpec myKeySpec = new DESKeySpec(keyAsBytes);
			SecretKeyFactory mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
			cipher = Cipher.getInstance(myEncryptionScheme);
			key = mySecretKeyFactory.generateSecret(myKeySpec);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] encryptedText = cipher.doFinal(plainText);
			//BASE64Encoder base64encoder = new BASE64Encoder();
			//encryptedString = base64encoder.encode(encryptedText);
			encryptedString=Base64.encodeBase64String(encryptedText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encryptedString;
	}
	
	public static void main(String[] args)
	{
		String text="{\"TransactionType\":\"CardWithdrawal\",\"ReferenceId\":\"110340228238684676\",\"Narration\":\"/rakesh/0729455784\",\"Amount\":200000,\"SourceAccount\":\"0100005526897\",\"Fee\":75,\"Currency\":\"KES\",\"Channel\":\"agent\",\"DestinationAccount\":\"0100006074914\",\"AgentIncomePercentage\":0.533}";
		String s1=DESEncryption.encrypt(text);
		System.out.println(s1);
		//String s2=DESEncryption.decrypt("4koKB53WLtOb/OoZM0iNVDYEeUkpPXltoZbhR77idRh8N9HgKfqJycfTJTSXMOLYzQbYh5tvjnj2acuKpEGVO+AWtg4Xq25ZLOiLh+umbRHWIAM0UmeDVVjIaZ7//YPto++dqO9JHjBJC7LtqK5fCsvd2ygTO6tl5HnCrTcLtt55C/1zDASN85R6m33ya9yasGO50zmngi6wqlY223imLklgUkCBYtn3qmlDEhVwDmLT/BOfjq8DqSCUx35OUOzUtUr//Bi0ZR0k1fRtvDGpynkT3XhhEdvb6EcqU9bBmnejNUhxfXxGhwuGneYozUXIscX++oNGFdmVCopL64PGUn5IbXJWoUps");
		String s2=DESEncryption.decrypt(s1);
		System.out.println(s2);
	
	}
}
