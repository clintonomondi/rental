package com.rental.common;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONObject;

import com.fasterxml.uuid.Generators;

import java.sql.Connection;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class GeneralCodes {

	Connection conn = null;

	public static String configFilePath() {
		//return "E:\\e-biller-files\\config.ini";
		return "/app/Project/config.ini";
	}

	public static String uuid_sys() {
		UUID uuid = Generators.randomBasedGenerator().generate();
		String ref_id = uuid.toString();
		return ref_id; 
	}
 
	public static String postRequest1(Hashtable<String, String> configs, String url, String m) {
//		 System.out.println("Json data::"+configs);
//		 System.out.println("url::"+url);
		try {
			boolean use_proxy; 
			Enumeration datesc;
 
			String httpsURL = url;
			URL myurl = new URL(httpsURL);

			if (url.contains("https")) {
			 System.getProperty("java.home");
				
				HttpsURLConnection conc = null;
				conc = (HttpsURLConnection) myurl.openConnection();

				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String data = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = configs.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					// System.out.println("Elements======" + str + ": " + configs.get(str));
					params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
					parent.put(str, configs.get(str));
				}

				// System.out.println("Working parent:: " + parent.toString());

				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (m) {
				case "POST":
					writer.write(params1.toString());
					break;
				case "JSON":
					writer.write(parent.toString());
					break;
				case "":
					break;
				}

				writer.flush();
				writer.close();
				os.close();

				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				}

				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);

				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					data = "" + inputLine;
				}

					return data.toString();

			} else {
				HttpURLConnection conc = null;
				conc = (java.net.HttpURLConnection) myurl.openConnection();

				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String data = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = configs.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					// System.out.println("Elements======" + str + ": " + configs.get(str));
					params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
					parent.put(str, configs.get(str));
				}
				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (m) {
				case "POST":
					writer.write(params1.toString());
					break;
				case "JSON":
					writer.write(parent.toString());
					break;
				case "":
					break;
				}
				writer.flush();
				writer.close();
				os.close();
				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				} else if (status == 503) {
					ins = conc.getErrorStream();
				} else {
					ins = conc.getErrorStream();
					// System.out.println(parent.toString());
					// System.out.println("step "+status+"===="+ins);
				}
//				System.out.println("step 2");
				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);
//				System.out.println("step 11");
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					data = "" + inputLine;
				}

				System.out.println("step 3");

				return data.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Hashtable<String,String> t=new Hashtable<String ,String>();
			t.put("responseCode","500");
			return t.toString();
		}
	}

	public static String postAccountValidationRequest1(Hashtable<String, String> configs, String url, String m) {
		// System.out.println("Json data::"+configs);
		try {
			boolean use_proxy;
			Enumeration datesc;

			String httpsURL = url;
			URL myurl = new URL(httpsURL);

			System.out.println("The httpsURL____" + httpsURL);

			System.out.println("myurl" + myurl);

			if (url.contains("https")) {
				System.out.println("Java path:___________" + System.getProperty("java.home"));
				// new HCertificate().certInfo();
				System.out.println("__________________________ _____________ _____________1 after certificate");

				HttpsURLConnection conc = null;
				conc = (HttpsURLConnection) myurl.openConnection();

				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String data = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = configs.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					// System.out.println("Elements======" + str + ": " + configs.get(str));
					params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
					parent.put(str, configs.get(str));
				}

//				System.out.println("Working parent:: " + parent.toString());

				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (m) {
				case "POST":

					writer.write(DESEncryption.encrypt(params1.toString()));
					break;
				case "JSON":
					writer.write(DESEncryption.encrypt(parent.toString()));
					break;
				case "":
					break;
				}

				writer.flush();
				writer.close();
				os.close();

				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				}

				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);

				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					data = "" + inputLine;

				}

				// System.out.println("_______________________________******************************_______________________________");
				// System.out.println(data);
				// System.out.println("_______________________________******************************_______________________________");
				return data.toString();

			} else {

				// new HCertificate().certInfo();
				System.out.println("____________________________________________________2");
				HttpURLConnection conc = null;
				conc = (java.net.HttpURLConnection) myurl.openConnection();

				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String data = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = configs.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					// System.out.println("Elements======" + str + ": " + configs.get(str));
					params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
					parent.put(str, configs.get(str));
				}
				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (m) {
				case "POST":
					// writer.write(params1.toString());
					writer.write(DESEncryption.encrypt(params1.toString()));
					break;
				case "JSON":
					// writer.write(parent.toString());
					writer.write(DESEncryption.encrypt(parent.toString()));
					break;
				case "":
					break;
				}
				writer.flush();
				writer.close();
				os.close();
				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				} else if (status == 503) {
					ins = conc.getErrorStream();
				} else {
					ins = conc.getErrorStream();
					// System.out.println(parent.toString());
					// System.out.println("step "+status+"===="+ins);
				}
				System.out.println("step 2");
				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);
				System.out.println("step 11");
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					System.out.println(inputLine);
					data = "" + inputLine;
				}

				System.out.println("step 3");

				return data.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Hashtable<String,String> t=new Hashtable<String ,String>();
			t.put("responseCode","500");
			System.out.println("Error 500 has occured>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			return t.toString();
		}
	}

	public static JSONObject jsonFormat(Hashtable<String, Object> configs) {

		boolean use_proxy;
		Enumeration datesc;

		OutputStream os = null;
		InputStream ins = null;
		List<NameValuePair> params1 = null;
		JSONObject parent = new JSONObject();
		String data = null;
		String str;
		datesc = configs.keys();
		params1 = new ArrayList<NameValuePair>();

		while (datesc.hasMoreElements()) {
			str = (String) datesc.nextElement();
			// System.out.println("Elements======" + str + ": " + configs.get(str));
			params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
			try {
				parent.put(str, "" + configs.get(str));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return parent;
	}

	public long cust_code() {
		long cust_number = 0;
		try {
			conn = DbManager.getConnection();
			CallableStatement prep = conn.prepareCall("{?=call `COMPANY_NO_GEN`()}");
			prep.registerOutParameter(1, Types.INTEGER);
			prep.execute();
			cust_number = prep.getInt(1);
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cust_number;
	}

	public static String encryptedPostRequest(Hashtable<String, String> configs, String url, String m) {
		// System.out.println("Json data::"+configs);
		try {
			boolean use_proxy;
			Enumeration datesc;

			String httpsURL = url;
			URL myurl = new URL(httpsURL);

			System.out.println("myurl" + myurl);

			if (url.contains("https")) {
				System.out.println("Java path:___________" + System.getProperty("java.home"));
			
				HttpsURLConnection conc = null;
				conc = (HttpsURLConnection) myurl.openConnection();

				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String data = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = configs.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					// System.out.println("Elements======" + str + ": " + configs.get(str));
					params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
					parent.put(str, configs.get(str));
				}

//				System.out.println("Working parent:: " + parent.toString());

				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (m) {
				case "POST":

					writer.write(DESEncryption.encrypt(params1.toString()));
					break;
				case "JSON":
					writer.write(DESEncryption.encrypt(parent.toString()));
					break;
				case "":
					break;
				}

				writer.flush();
				writer.close();
				os.close();

				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				}

				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);

				String inputLine;

				while ((inputLine = in.readLine()) != null) {
//					System.out.println(inputLine);
					data = "" + inputLine;

				}

					return data.toString();

			} else {

				HttpURLConnection conc = null;
				conc = (java.net.HttpURLConnection) myurl.openConnection();

				OutputStream os = null;
				InputStream ins = null;
				List<NameValuePair> params1 = null;
				JSONObject parent = new JSONObject();
				String data = null;

				conc.setConnectTimeout(15000);
				conc.setRequestMethod("POST");
				conc.setDoInput(true);
				conc.setDoOutput(true);
				conc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				String str;
				datesc = configs.keys();
				params1 = new ArrayList<NameValuePair>();

				while (datesc.hasMoreElements()) {
					str = (String) datesc.nextElement();
					// System.out.println("Elements======" + str + ": " + configs.get(str));
					params1.add(new BasicNameValuePair(str, "" + configs.get(str)));
					parent.put(str, configs.get(str));
				}
//				System.out.println("Working parent:: " + DESEncryption.encrypt(parent.toString()));//Log 4 j here
				os = conc.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
				switch (m) {
				case "POST":
					// writer.write(params1.toString());
					writer.write(DESEncryption.encrypt(params1.toString()));
					break;
				case "JSON":
					// writer.write(parent.toString());
					writer.write(DESEncryption.encrypt(parent.toString()));
					break;
				case "":
					break;
				}
				writer.flush();
				writer.close();
				os.close();
				int status = conc.getResponseCode();
				if (status == 200) {
					ins = conc.getInputStream();
				} else if (status == 404) {
					ins = conc.getErrorStream();
				} else if (status == 503) {
					ins = conc.getErrorStream();
				} else {
					ins = conc.getErrorStream();
					// System.out.println(parent.toString());
					// System.out.println("step "+status+"===="+ins);
				}
//				System.out.println("step 2");
				java.io.InputStreamReader isr = new java.io.InputStreamReader(ins);
				java.io.BufferedReader in = new java.io.BufferedReader(isr);
//				System.out.println("step 11");
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
//					System.out.println(inputLine);
					data = "" + inputLine;
				}

//				System.out.println("step 3");

				return data.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Hashtable<String,String> t=new Hashtable<String ,String>();
			t.put("responseCode","500");
			System.out.println("Error 500 has occured>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			return t.toString();
		}
	}

}
