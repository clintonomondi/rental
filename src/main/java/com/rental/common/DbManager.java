package com.rental.common;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.ini4j.Ini;

//import java.sql.Connection;;
//import com.mysql.jdbc.Driver;

public class DbManager {

	public static Connection con;
	
	public static String url;
	public static String port ;
	public static String database;
	public static String host;
	public static String dbusername;
	public static String dbpassword;
	
	//static Logger log = Logger.getLogger(DbManager.class.getName());

	public DbManager() {

	}
	public static Connection getConnection() {
////		String password="";
		try {
		       Ini ini;   ini = new Ini(new File(GeneralCodes.configFilePath()));
			   port=ini.get("header", "port");
			   database=ini.get("header", "database");
			   host=ini.get("header", "host");
			   url = "jdbc:mysql://"+host+":"+port+"/"+database+"?autoReconnect=true&useSSL=false"; 
			   dbusername=ini.get("header", "db_username");
			   dbpassword=ini.get("header", "db_password");
			   
			 //  System.out.println(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		dbpassword=new EncryptionManager().decrypt(dbpassword) ;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Properties properties = new Properties();
			properties.put("user", dbusername);
			properties.put("password", dbpassword);
			con=DriverManager.getConnection(url, properties);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("db"+e.getMessage());
		}
		
		return con;
	}
}
